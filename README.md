# struts-showcase
The purpose of this repo is to provide example pipeline scripts for GitLab. This repo contains the source code and Maven build instructions (pom.xml) for the Struts2 showcase web app. This web app was released with Apache Struts v2.5.28. This choice is somewhat arbitrary, because Spectra Assure is capable of scanning nearly any type of software artifact that results from a build.

**.gitlab-ci.yml**

This pipeline works in GitLab CI/CD runners. The pipeline has two stages - "build" and "test". The build stage has 1 job that builds the .war file. The test stage has 1 job that scans the .war using the Spectra Assure Docker image. YAML provided by ReversingLabs ("rl-scanner-gitlab-include.yml") is used to handle kicking off the scan and saving the reports as job artifacts.